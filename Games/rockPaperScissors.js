module.exports ={
    
    name : "Rock Paper Scissors",
    description : "plays the game",
    execute(message, args){
        if (message.author.bot) return;
        var member = message.mentions.members.first(); 
        // Game Rock Paper Scissors
    const msgToLower = message.content.toLowerCase();
    // Create a gamearray with 3 strings
    const gameArray = ["rock", "paper", "scissors"];
    // Create a variable and store the number 3
    const multiply = 3;
    // Check Input
    const inputArray = ["rock", "paper", "scissors"];
    
    const wonArray = ["It's a draw!", "The computer wins!", "You win!"];    
    const chosenArray = ["You've chosen rock!", "You've chosen paper!", "You've chosen scissors!"];
    const pcChosen = "Pc has chosen: ";
 

    // Check if the user input is rock
    if (msgToLower === inputArray[0]){
        // Print a visual message (in-efficient)
        message.reply(chosenArray[0]);
        // Create a random number between 0-2
        const random = Math.floor(Math.random() * multiply);        
        // Select an item from the array with the random generated number
        const pcSelect = gameArray[random];        
        // print a message letting the user know what the computer choice is (in-efficient)
        message.reply(pcChosen + pcSelect);
        // if the pc selected rock
        if (pcSelect === gameArray[0]){
            message.reply(wonArray[0]);
        }
        // if the pc selected paper
        else if (pcSelect === gameArray[1]){
            message.reply(wonArray[1]);
        }
        // if the pc selected scissors
        else if (pcSelect === gameArray[2]){
            message.reply(wonArray[2]);
        }
    }

    // Check if the user input is paper
    else if (msgToLower === inputArray[1]){
        // Print a visual message (in-efficient)
        message.reply(chosenArray[1]);
        // Create a random number between 0-2
        const random = Math.floor(Math.random() * multiply);        
        // Select an item from the array with the random generated number
        const pcSelect = gameArray[random];        
        // print a message letting the user know what the computer choice is (in-efficient)
        message.reply(pcChosen + pcSelect);
        // if the pc selected rock
        if (pcSelect === gameArray[0]){
            message.reply(wonArray[2]);
        }
        // if the pc selected paper
        else if (pcSelect === gameArray[1]){
            message.reply(wonArray[0]);
        }
        // if the pc selected scissors
        else if (pcSelect === gameArray[2]){
            message.reply(wonArray[1]);
        }
    }

    // Check if the user input is scissors
    else if (msgToLower === inputArray[2]){
        // Print a visual message (in-efficient)
        message.reply(chosenArray[2]);
        // Create a random number between 0-2
        const random = Math.floor(Math.random() * multiply);       
        // Select an item from the array with the random generated number
        const pcSelect = gameArray[random];        
        // print a message letting the user know what the computer choice is (in-efficient)
        message.reply(pcChosen + pcSelect);
        // if the pc selected rock
        if (pcSelect === gameArray[0]){
            message.reply(wonArray[1]);
        }
        // if the pc selected paper
        else if (pcSelect === gameArray[1]){
            message.reply(wonArray[2]);
        }
        // if the pc selected scissors
        else if (pcSelect === gameArray[2]){
            message.reply(wonArray[0]);
        }
    }
    }
}