

    //if(!message.member.roles.cache.find(r => r.name === 'Moderator'))
      //  message.reply("You do not have the permission");

  module.exports ={
    name : "kick",
    description : "kicks member out of the server",
    execute(message, args){
      
      var member = message.mentions.members.first(); 
    if(message.content.toLowerCase().startsWith("kick")){  

    var member = message.mentions.members.first();

    // Checks if the member name is present on the server.
    if(!member)
      return message.reply("Please enter a name that is on the server");
    // If the member to be kicked has a higher authority than the bot it prints the message.
    if(!member.kickable) 
      return message.reply(`Unfortunately, I don't have the permission to kick ${member.user.tag}`);

    // Kicks the mentioned member.
    member.kick().then(() => {
        message.channel.send(member.displayName + `has been successfully kicked by ${message.author.tag}`);
    }).catch(e => {
        message.channel.send(`Sorry ${message.author} I couldn't kick because of an error`);
     });    
    }
  }
}
    