module.exports ={
    name : "bannedWords",
    description : "Deletes the message if the user swears",
    execute(message, args){   
        if (message.author.bot) return; 

        for(i=0 ; i<bannedWords.length; i++){
            if(message.content.toLowerCase().includes(bannedWords[i])) {
                message.delete();
                message.reply("Do not swear");
                return;
            }
        }        
    }
}
