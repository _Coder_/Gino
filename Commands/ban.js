module.exports ={
    name : "ban",
    description : "kicks member out of the server",
    execute(message, args){
      if (message.author.bot) return;
      var member = message.mentions.members.first(); 
        if(message.content.toLowerCase().startsWith("ban")){  

            var member = message.mentions.members.first();
        
            // Checks if the member name is present on the server.
            if(!member)
              return message.reply("Please enter a name that is on the server");
            // If the member to be kicked has a higher authority than the bot it prints the message.
            if(!member.bannable) 
              return message.reply(`Unfortunately, I don't have the permission to ban ${member.user.tag}`);
        
            // Bans the mentioned member.
            member.ban().then(() => {
                message.channel.send(member.displayName + `has been successfully banned by ${message.author.tag}`);
            }).catch(e => {
                message.channel.send(`Sorry ${message.author} I couldn't kick because of an error`);
             });       
        }
    }
}