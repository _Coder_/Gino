// Require discord.js package
const Discord = require("discord.js");
// Create a new client using the new keyword
const client = new Discord.Client();
const prefix = config.prefix;
const config = require("./config.json");

const fs = require("fs");

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./Commands/').filter(files => files.endsWith('.js'));
for(const file of commandFiles){
    const command = require(`./Commands/${file}`);
    client.commands.set(command.name, command);
}


// If config.json has not been filled, it will 
// remind the user to do so.
if(config.token == " " || config.prefix == " "){
    console.log("Please fill the config.json file");
    process.exit(1);
}


let bannedWords = "fuck,shit,slut,whore".split(",");



// Display a message once the bot has started ()
client.on('ready', () =>{
    console.log(`Logged in as ${client.user.tag}!`);
});



//
client.on('guildMemberAdd', member => {    
                                                                                 //Channel Name
    const channel = member.guild.channels.cache.find(channel => channel.name === "welcome")
    if(!channel)return;

    channel.send(`${member} welcome to the server!`);

    });  

client.on("message", message => {
    if (message.author.bot) return;
    const args = message.content.slice(prefix.length).split(/ +/);
	const args = message.content.toLocaleLowerCase();
	
	//if (!message.content.startsWith(prefix)) return;

    switch (args[0]){
        case  "kick" :
            client.commands.execute(message, args);
        break;


    }
    
}); 



// Log in the bot with the token
client.login(config.token);