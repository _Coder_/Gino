# Gino - Discord Bot

A simple discord bot that helps you manage your server by providing useful moderation commands and also allows friends to play simple and fun games together without being in the same room.. Gino is an fully customizable Discord bot that is constantly growing. 

# List of Commands
- **_For moderation_**
    - kick - kicks an tagged member out of the server, happens after they have been muted and still don't follow the guidelines.
    - ban - bans an tagged member from the server.
    - banwords - a comprehensive list of banned words.
    - mute - mutess an tagged member on the server.
    - unmute - unmutess the tagged member.
- **_For fun_**
    - jokes - tells a random joke about someone, you can tag a friend.
    - rockpaperscissors - plays rock, paper, scissors with the user.
    - hangman - this program includes features like over 5000 playable words in total and allows input from multiple users for      server-wide play.

# Installation and Setup
1. Go to Discord's [Developer Portal](https://discord.com/developers/applications/).
2. Create a new application.
3. Take note of your bot's client ID. You will need this to invite your bot to a server.
4. Invite your bot to a server using [this](Invite your bot to a server using).
5. Alternatively, you can clone this repo and host the bot yourself

    https://gitlab.com/_Coder_/Gino`

6. After cloning, run 

    `npm install`

   to install all dependencies.

# Requirements 
- [Node](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/)
- [Discord guide](https://discordjs.guide/preparations/setting-up-a-bot-application.html)

# config.json

You need to personalize your own config with the following information:
- your own bot command prefix
- your discord bot token

`{
    "token":"ADD YOUR TOKEN",
    "prefix":"ADD YOUR PREFIX"
}
`





